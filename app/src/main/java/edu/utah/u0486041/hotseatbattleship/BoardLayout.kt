package edu.utah.u0486041.hotseatbattleship

import android.content.Context
import android.graphics.PointF
import android.graphics.Rect
import android.view.View
import android.view.ViewGroup

/**
 * Created by Lance on 10/25/2016.
 */
class BoardLayout(context: Context) : ViewGroup(context) {
    private val _board = Array(10) { arrayOfNulls<CellView?>(10) }

    val boardHeight: Int
        get() = _board.size

    val boardWidth: Int
        get() = _board.size

    override fun addView(child: View) {
        if (childCount < _board.size * _board.size) {
            super.addView(child)
            if (child is CellView) {
                val cellView = child
                val x = cellView.cellCoordinates.x
                val y = cellView.cellCoordinates.y
                _board[y][x] = cellView
            }
        }
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        for (childIndex in 0..childCount - 1) {

            var width = width.toFloat()
            var height = height.toFloat()

            if (height < width) {
                width = height
            } else {
                height = width
            }

            val childWidth = width / _board.size
            val childHeight = height / _board.size
            var x = 0
            var y = 0

            val childCenter = PointF()
            if (getChildAt(childIndex) is CellView) {
                val cellView = getChildAt(childIndex) as CellView
                x = cellView.cellCoordinates.x
                y = cellView.cellCoordinates.y
            }

            childCenter.x = x.toFloat() * childWidth + childWidth * 0.5f
            childCenter.y = y.toFloat() * childHeight + childHeight * 0.5f

            val childRect = Rect()
            childRect.left = (childCenter.x - childWidth * 0.5f).toInt()
            childRect.right = (childCenter.x + childWidth * 0.5f).toInt()
            childRect.top = (childCenter.y - childHeight * 0.5f).toInt()
            childRect.bottom = (childCenter.y + childHeight * 0.5f).toInt()

            val childView = getChildAt(childIndex)
            childView.layout(childRect.left, childRect.top, childRect.right, childRect.bottom)
        }
    }

    fun setBoard(board: Array<Array<GameObject.CellState>>, hideShips: Boolean) {
        for (i in board.indices) {
            for (j in 0..board[i].size - 1) {
                if (board[i][j] == GameObject.CellState.SHIP && hideShips) {
                    updateBoard(j, i, GameObject.CellState.WATER)
                } else {
                    updateBoard(j, i, board[i][j])
                }
            }
        }
    }

    fun updateBoard(x: Int, y: Int, value: GameObject.CellState) {
        _board[y][x]?.state = value
    }

}
