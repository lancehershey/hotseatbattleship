package edu.utah.u0486041.hotseatbattleship

import android.content.res.Configuration
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout

/**
 * Created by Lance on 10/25/2016.
 */
class GameScreen : Fragment() {
    companion object {
        fun newInstance(): GameScreen {
            return GameScreen()
        }
    }

    private var playerBoard: BoardLayout? = null
    private var opponentBoard: BoardLayout? = null

    private fun generateBoard(boardLayout: BoardLayout?, clickable: Boolean) {
        if (boardLayout != null) {
            for (i in 0..boardLayout.boardHeight - 1) {
                for (j in 0..boardLayout.boardWidth - 1) {
                    val cellView = CellView(context)
                    cellView.setCellCoordinates(j, i) // x = j, y = i;
                    if (clickable) {
                        cellView.setOnCellSelectedListener(activity as GameScreenActivity)
                    }
                    boardLayout.addView(cellView)
                }
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val game = GameManager.getGame(GameManager.currentGame)

        val screenLayout = LinearLayout(context)
        val playerBoardLayoutParams: LinearLayout.LayoutParams
        val opponentBoardLayoutParams: LinearLayout.LayoutParams

        if (resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            screenLayout.orientation = LinearLayout.HORIZONTAL
            playerBoardLayoutParams = LinearLayout.LayoutParams(
                    0,
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    1f
            )
            opponentBoardLayoutParams = LinearLayout.LayoutParams(
                    0,
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    3f
            )
        } else {
            screenLayout.orientation = LinearLayout.VERTICAL
            playerBoardLayoutParams = LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    0,
                    1f
            )
            opponentBoardLayoutParams = LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    0,
                    3f
            )
        }

        playerBoard = BoardLayout(context)
        generateBoard(playerBoard, false)
        playerBoard?.setBoard(game.getPlayerBoard(), false)
        screenLayout.addView(playerBoard, playerBoardLayoutParams)

        opponentBoard = BoardLayout(context)
        generateBoard(opponentBoard, true)
        opponentBoard?.setBoard(game.getOpponentBoard(), true)
        screenLayout.addView(opponentBoard, opponentBoardLayoutParams)

        return screenLayout
    }

    fun updatePosition(player: Int, x: Int, y: Int, state: GameObject.CellState) {
        when(player) {
            0 -> playerBoard?.updateBoard(x, y, state)
            1 -> opponentBoard?.updateBoard(x, y, state)
            else -> Log.e("GameScreen updateBoard", "ERROR, unexpected player value: $player")
        }
    }

    fun updateBoard(game: GameObject) {
        playerBoard?.setBoard(game.getPlayerBoard(), false)
        opponentBoard?.setBoard(game.getOpponentBoard(), true)
    }

}
