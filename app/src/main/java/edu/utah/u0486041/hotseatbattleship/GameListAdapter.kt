package edu.utah.u0486041.hotseatbattleship

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.recyclerview_item.view.*

/**
 * Created by Lance on 11/3/2017.
 */
class GameListAdapter : RecyclerView.Adapter<GameListAdapter.GameSummaryHolder>() {

    override fun onBindViewHolder(holder: GameListAdapter.GameSummaryHolder, position: Int) {
        val itemGameIndex: Int = position
        holder.bindGame(itemGameIndex)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GameListAdapter.GameSummaryHolder {
        val inflatedView = LayoutInflater.from(parent.context).inflate(R.layout.recyclerview_item, parent, false)
        return GameSummaryHolder(inflatedView)
    }

    override fun getItemCount(): Int {
        return GameManager.gameCount
    }

    class GameSummaryHolder(v: View) : RecyclerView.ViewHolder(v), View.OnClickListener {
        private var view = v
        private var gameIndex: Int = 0

        init {
            view.setOnClickListener(this)
        }

        fun bindGame(gameIndex: Int) {
            this.gameIndex = gameIndex
            val gameNumberText = "Game $gameIndex"
            view.item_game_number.text = gameNumberText
            val gameStatusText: String
            val gameInfoText: String
            val player1ShipsRemaining: String = "Player 1 ships remaining: ${GameManager.getGame(gameIndex).shipsRemaining(0)}"
            val player2ShipsRemaining: String = "Player 2 ships remaining: ${GameManager.getGame(gameIndex).shipsRemaining(1)}"
            when (GameManager.getGame(gameIndex).getGameState()) {
                GameObject.GameState.INITIALIZING -> {
                    gameStatusText = "STATUS: Game Starting"
                    gameInfoText = "Initializing"
                }
                GameObject.GameState.PLAYER1_PLACING_SHIPS -> {
                    gameStatusText = "STATUS: Game Starting"
                    gameInfoText = "Waiting for Player 1 to place ships"
                }
                GameObject.GameState.PLAYER2_PLACING_SHIPS -> {
                    gameStatusText = "STATUS: Game Starting"
                    gameInfoText = "Waiting for Player 2 to place ships"
                }
                GameObject.GameState.PLAYER1_TURN -> {
                    gameStatusText = "STATUS: In Progress"
                    gameInfoText = "Player 1's Turn"
                }
                GameObject.GameState.PLAYER2_TURN -> {
                    gameStatusText = "STATUS: In Progress"
                    gameInfoText = "Player 2's Turn"
                }
                GameObject.GameState.PLAYER1_WIN -> {
                    gameStatusText = "STATUS: Game Over"
                    gameInfoText = "Winner: Player 1"
                }
                GameObject.GameState.PLAYER2_WIN -> {
                    gameStatusText = "STATUS: Game Over"
                    gameInfoText = "Winner: Player 2"
                }
            }
            view.item_game_state.text = gameStatusText
            view.item_game_info.text = gameInfoText
            view.item_player1_ships_remaining.text = player1ShipsRemaining
            view.item_player2_ships_remaining.text = player2ShipsRemaining
        }

        override fun onClick(v: View) {
            // Launch if the game is still in progress. Don't if the game is over.
            if (GameManager.getGame(gameIndex).getGameState() == GameObject.GameState.PLAYER1_WIN
                    || GameManager.getGame(gameIndex).getGameState() == GameObject.GameState.PLAYER2_WIN) {
                return
            }
            val launchGameIntent = Intent(itemView.context, GameScreenActivity::class.java)
            launchGameIntent.putExtra(R.string.GAME_KEY.toString(), gameIndex)
            itemView.context.startActivity(launchGameIntent)
        }

    }

}