package edu.utah.u0486041.hotseatbattleship

import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

/**
 * Created by Lance on 11/1/2016.
 */
class OverlayScreen : Fragment() {
    companion object {
        fun newInstance(): OverlayScreen {
            return OverlayScreen()
        }
    }

    private var textView: TextView? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        textView = TextView(container?.context)
        textView?.id = View.generateViewId()
        textView?.setBackgroundColor(Color.BLACK)
        textView?.setTextColor(Color.WHITE)
        textView?.text = arguments.getString(R.string.OVERLAY_MESSAGE_KEY.toString())
        textView?.textAlignment = View.TEXT_ALIGNMENT_CENTER
        textView?.gravity = Gravity.CENTER_VERTICAL
        textView?.textSize = 24.0f
        textView?.setOnClickListener(activity as GameScreenActivity)

        // This needs to be the fragment view we want to see.
        return textView
    }
}
