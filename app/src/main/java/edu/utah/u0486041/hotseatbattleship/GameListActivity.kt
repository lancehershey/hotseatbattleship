package edu.utah.u0486041.hotseatbattleship

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.View
import kotlinx.android.synthetic.main.activity_game_list.*

/**
 * Created by Lance on 11/4/2017.
 */
class GameListActivity : AppCompatActivity(), View.OnClickListener {
    private lateinit var adapter: GameListAdapter
    private lateinit var layoutManager: GridLayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game_list)

        new_game_button.setOnClickListener(this)

        adapter = GameListAdapter()
        layoutManager = GridLayoutManager(this, 2)
        recycler_view.adapter = adapter
        recycler_view.layoutManager = layoutManager
        setRecyclerViewItemTouchListener()

        GameManager.loadFile(this)
        recycler_view.adapter.notifyDataSetChanged()
    }

    override fun onRestart() {
        super.onRestart()
        recycler_view.adapter.notifyDataSetChanged()
    }

    override fun onClick(view: View?) {
        val newGameIntent = Intent(this, GameScreenActivity::class.java)
        startActivity(newGameIntent)
    }

    // The code for swipe to delete was modified from https://www.raywenderlich.com/170075/android-recyclerview-tutorial-kotlin
    private fun setRecyclerViewItemTouchListener() {
        val itemTouchCallback = object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT) {
            override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, viewHolder1: RecyclerView.ViewHolder): Boolean {
                return false
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, swipeDir: Int) {
                val position = viewHolder.adapterPosition
                GameManager.deleteGame(position)
                recycler_view.adapter.notifyItemRemoved(position)
                GameManager.saveFile(this@GameListActivity)
            }
        }
        val itemTouchHelper = ItemTouchHelper(itemTouchCallback)
        itemTouchHelper.attachToRecyclerView(recycler_view)
    }

}