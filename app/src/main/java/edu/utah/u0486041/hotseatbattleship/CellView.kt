package edu.utah.u0486041.hotseatbattleship

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View

/**
 * Created by Lance on 10/27/2017.
 */
class CellView: View {
    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes)

    interface OnCellSelectedListener {
        fun onCellSelected(cellView: CellView)
    }

    var state: GameObject.CellState = GameObject.CellState.WATER
        set(newState) {
            field = newState
            invalidate()
        }
    val cellCoordinates = Point()

    fun setCellCoordinates(x: Int, y: Int) {
        cellCoordinates.x = x
        cellCoordinates.y = y
    }

    private var onCellSelectedListener: OnCellSelectedListener? = null

    private val borderPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        color = Color.BLACK
        style = Paint.Style.STROKE
        strokeWidth = 5.0f
    }
    private val fillPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        color = Color.BLUE
        style = Paint.Style.FILL
    }
    private val pegPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        style = Paint.Style.FILL
    }
    private val cellRect = RectF()

    fun setOnCellSelectedListener(listener: OnCellSelectedListener) {
        onCellSelectedListener = listener
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        setMeasuredDimension(MeasureSpec.getSize(widthMeasureSpec),
                MeasureSpec.getSize(heightMeasureSpec))
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        onCellSelectedListener?.onCellSelected(this)

        return super.onTouchEvent(event)
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)

        if (canvas !is Canvas) {
            return
        }

        when (state) {
            GameObject.CellState.WATER -> fillPaint.color = Color.BLUE
            GameObject.CellState.SHIP -> fillPaint.color = Color.GRAY
            GameObject.CellState.MISS -> {
                fillPaint.color = Color.BLUE
                pegPaint.color = Color.WHITE
            }
            GameObject.CellState.HIT -> {
                fillPaint.color = Color.GRAY
                pegPaint.color = Color.RED
            }
            GameObject.CellState.SUNK -> {
                fillPaint.color = Color.BLACK
                pegPaint.color = Color.RED
            }
        }

        // Using size in both places will make it square
        val size = if (width < height) width else height
        cellRect.left = paddingLeft.toFloat()
        cellRect.top = paddingTop.toFloat()
        cellRect.right = size - paddingRight.toFloat()
        cellRect.bottom = size - paddingBottom.toFloat()

        // Fill first
        canvas.drawRect(cellRect, fillPaint)

        // Draw a peg on top of fill color if needed
        if (state == GameObject.CellState.MISS || state == GameObject.CellState.HIT || state == GameObject.CellState.SUNK) {
            canvas.drawOval(cellRect, pegPaint)
        }

        // Always draw the border on top of everything else
        canvas.drawRect(cellRect, borderPaint)
    }
}