package edu.utah.u0486041.hotseatbattleship

import android.util.Log
import android.util.SparseArray
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.util.*

/**
 * Created by Lance on 10/26/2017.
 */
class GameObject {
    companion object {
        private val player1BoardJSONKey: String = "player1"
        private val player2BoardJSONKey: String = "player2"
        private val carrierJSONKey: String = "carrier"
        private val battleshipJSONKey: String = "battleship"
        private val cruiserJSONKey: String = "cruiser"
        private val submarineJSONKey: String = "submarine"
        private val destroyerJSONKey: String = "destroyer"
        private val gameStateJSONKey: String = "state"

        private val carrierSize: Int = 5
        private val battleshipSize: Int = 4
        private val cruiserSize: Int = 3
        private val submarineSize: Int = 3
        private val destroyerSize: Int = 2

        fun fromJSON(JSON: String): GameObject? {
            val gameObjectJSON = JSONObject(JSON)
            return try {
                val game = GameObject()
                val state: GameState = GameState.fromString(gameObjectJSON.getString(gameStateJSONKey)) as GameState
                val boards = Array(2, { SparseArray<CellState>() })

                val player1Board: JSONArray = gameObjectJSON.getJSONArray(player1BoardJSONKey)
                for (i in 0..player1Board.length() - 1) {
                    val obj = player1Board.getJSONObject(i)
                    val index: Int = obj.getInt("index")
                    val cellState: CellState = CellState.fromString(obj.getString("state")) as CellState
                    boards[0].put(index, cellState)
                }

                val player2Board: JSONArray = gameObjectJSON.getJSONArray(player2BoardJSONKey)
                for (i in 0..player2Board.length() - 1) {
                    val obj = player2Board.getJSONObject(i)
                    val index: Int = obj.getInt("index")
                    val cellState: CellState = CellState.fromString(obj.getString("state")) as CellState
                    boards[1].put(index, cellState)
                }

                val carrier = Array(2, { IntArray(carrierSize) })
                val carrierJSON = gameObjectJSON.get(carrierJSONKey) as JSONArray
                for (i in 0..carrierSize - 1) {
                    carrier[0][i] = carrierJSON.getJSONArray(0).getInt(i)
                    carrier[1][i] = carrierJSON.getJSONArray(1).getInt(i)
                }

                val battleship = Array(2, { IntArray(battleshipSize) })
                val battleshipJSON = gameObjectJSON.get(battleshipJSONKey) as JSONArray
                for (i in 0..battleshipSize - 1) {
                    battleship[0][i] = battleshipJSON.getJSONArray(0).getInt(i)
                    battleship[1][i] = battleshipJSON.getJSONArray(1).getInt(i)
                }

                val cruiser = Array(2, { IntArray(cruiserSize) })
                val cruiserJSON = gameObjectJSON.get(cruiserJSONKey) as JSONArray
                for (i in 0..cruiserSize - 1) {
                    cruiser[0][i] = cruiserJSON.getJSONArray(0).getInt(i)
                    cruiser[1][i] = cruiserJSON.getJSONArray(1).getInt(i)
                }

                val submarine = Array(2, { IntArray(submarineSize) })
                val submarineJSON = gameObjectJSON.get(submarineJSONKey) as JSONArray
                for (i in 0..submarineSize - 1) {
                    submarine[0][i] = submarineJSON.getJSONArray(0).getInt(i)
                    submarine[1][i] = submarineJSON.getJSONArray(1).getInt(i)
                }

                val destroyer = Array(2, { IntArray(destroyerSize) })
                val destroyerJSON = gameObjectJSON.get(destroyerJSONKey) as JSONArray
                for (i in 0..destroyerSize - 1) {
                    destroyer[0][i] = destroyerJSON.getJSONArray(0).getInt(i)
                    destroyer[1][i] = destroyerJSON.getJSONArray(1).getInt(i)
                }

                game.loadGame(state, boards, carrier, battleship, cruiser, submarine, destroyer)
                return game
            } catch (exception: JSONException) {
                null
            }
        }
    }

    fun toJSON(): String {
        val gameObjectJSON = JSONObject()
        val player1BoardJSON = JSONArray()
        val player2BoardJSON = JSONArray()

        for (i in 0.._gameBoards[0].size() - 1) {
            val index: Int = _gameBoards[0].keyAt(i)
            val state: CellState = _gameBoards[0].get(index)
            player1BoardJSON.put(JSONObject().put("index", index).put("state", state.name))
        }
        for (i in 0.._gameBoards[1].size() - 1) {
            val index: Int = _gameBoards[1].keyAt(i)
            val state: CellState = _gameBoards[1].get(index)
            player2BoardJSON.put(JSONObject().put("index", index).put("state", state.name))
        }
        gameObjectJSON.put(gameStateJSONKey, _gameState.name)
        gameObjectJSON.put(player1BoardJSONKey, player1BoardJSON)
        gameObjectJSON.put(player2BoardJSONKey, player2BoardJSON)
        gameObjectJSON.put(carrierJSONKey, JSONArray(_carrier))
        gameObjectJSON.put(battleshipJSONKey, JSONArray(_battleship))
        gameObjectJSON.put(cruiserJSONKey, JSONArray(_cruiser))
        gameObjectJSON.put(submarineJSONKey, JSONArray(_submarine))
        gameObjectJSON.put(destroyerJSONKey, JSONArray(_destroyer))

        Log.e("GameObject JSON", gameObjectJSON.toString())
        return gameObjectJSON.toString()
    }

    interface OnUpdateGameStateListener {
        fun onPositionUpdate(player: Int, position: Int, state: CellState)
        fun onStateChanged(state: GameState, cell: Int)
    }

    enum class CellState {
        WATER,
        SHIP,
        MISS,
        HIT,
        SUNK;

        // Companion object for doing a reverse lookup when parsing JSON
        // Modified from https://stackoverflow.com/questions/37794850/effective-enums-in-kotlin-with-reverse-lookup
        companion object {
            private val map = CellState.values().associateBy(CellState::name)
            fun fromString(state: String) = map[state]
        }
    }

    enum class GameState {
        INITIALIZING,
        PLAYER1_PLACING_SHIPS,
        PLAYER2_PLACING_SHIPS,
        PLAYER1_TURN,
        PLAYER2_TURN,
        PLAYER1_WIN,
        PLAYER2_WIN;

        companion object {
            private val map = GameState.values().associateBy(GameState::name)
            fun fromString(state: String) = map[state]
        }
    }

    private var _gameBoards = Array (2, { SparseArray<CellState>() })
    private var _validBoard = false
    private var _gameState: GameState = GameState.INITIALIZING

    private var onUpdateGameStateListener: OnUpdateGameStateListener? = null

    // These arrays are filled with their board positions
    // carrier[0] is player 1's carrier and carrier[1] is player 2's, etc.
    private var _carrier = Array(2, { IntArray(carrierSize) })
    private var _battleship = Array(2, { IntArray(battleshipSize) })
    private var _cruiser = Array(2, { IntArray(cruiserSize) })
    private var _submarine = Array(2, { IntArray(submarineSize) })
    private var _destroyer = Array(2, { IntArray(destroyerSize) })

    init {
        _gameState = GameState.PLAYER1_PLACING_SHIPS
        setup(0)
        logBoard(0)
        _gameState = GameState.PLAYER2_PLACING_SHIPS
        setup(1)
        logBoard(1)
        _gameState = GameState.PLAYER1_TURN
    }

    fun setOnUpdateGameStateListener(listener: OnUpdateGameStateListener) {
        onUpdateGameStateListener = listener
    }

    fun loadGame(state: GameState, boards: Array<SparseArray<CellState>>, carrier: Array<IntArray>,
                 battleship: Array<IntArray>, cruiser: Array<IntArray>, submarine: Array<IntArray>,
                 destroyer: Array<IntArray>) {
        _gameState = state
        _gameBoards = boards
        _carrier = carrier
        _battleship = battleship
        _cruiser = cruiser
        _submarine = submarine
        _destroyer = destroyer
    }

    private fun setup(player: Int) {
        _validBoard = false
        while (!_validBoard) {
            _gameBoards[player].clear()
            placeShipsRandomly(player)
        }
    }

    private fun placeShipsRandomly(player: Int) {
        placeShip(player, _carrier[player]) // This placement is first, so it should always be valid.
        placeShip(player, _battleship[player])
        if (!_validBoard) return
        placeShip(player, _cruiser[player])
        if (!_validBoard) return
        placeShip(player, _submarine[player])
        if (!_validBoard) return
        placeShip(player, _destroyer[player])
        // Even if the board is invalid, this will return, so no check here.
    }

    private fun placeShip(player: Int, ship: IntArray) {
        // SparseArray index to grid conversion.
        //  0  1  2  3  4  5  6  7  8  9
        // 10 11 12 13 14 15 16 17 18 19
        // 20 21 22 23 24 25 26 27 28 29
        // 30 31 32 33 34 35 36 37 38 39
        // 40 41 42 43 44 45 46 47 48 49
        // 50 51 52 53 54 55 56 57 58 59
        // 60 61 62 63 64 65 66 67 68 69
        // 70 71 72 73 74 75 76 77 78 79
        // 80 81 82 83 84 85 86 87 88 89
        // 90 91 92 93 94 95 96 97 98 99
        // This is convenient because moving horizontally means incrementing by 1
        // and moving vertically means incrementing by 10.

        val rand = Random()
        val horizontal = rand.nextBoolean()
        val x: Int
        val y: Int
        val step: Int
        if (horizontal) {
            step = 1
            // If horizontal, pick a starting row from 0 to 9
            x = rand.nextInt(10) * 10
            // Then pick a starting column from 0 to 9 - ship size
            y = rand.nextInt(10 - ship.size)
        } else {
            step = 10
            // If vertical, pick a starting row from 0 to 9 - ship size
            x = rand.nextInt(10 - ship.size) * 10
            // Then pick a starting column from 0 to 9
            y = rand.nextInt(10)
        }
        // Now assign the positions for the ship that are guaranteed to be in range.
        for (i in 0..ship.size - 1) {
            ship[i] = x + y + (i * step)
            // If we try to assign a position that has already been assigned, we just invalidate the
            // board and start over instead of trying to fix the current ship.
            if (_gameBoards[player].get(ship[i]) != null) {
                _validBoard = false
                return
            }
        }
        // Once here, we've established that the ship doesn't overlap with any other ships.
        _validBoard = true
        // Finally, commit the ship positions to the game board
        for (i in 0..ship.size - 1) {
            _gameBoards[player].put(ship[i], CellState.SHIP)
        }
    }

    fun getCellState(player: Int, index: Int): CellState {
        if (_gameBoards[player].get(index) != null) {
            return _gameBoards[player].get(index)
        } else {
            return CellState.WATER
        }
    }

    fun getGameState(): GameState {
        return _gameState
    }

    fun getShipName(player: Int, position: Int): String {
        val shipName: String
        if (_carrier[player].contains(position)) {
            shipName = "Carrier"
        } else if (_battleship[player].contains(position)) {
            shipName = "Battleship"
        } else if (_cruiser[player].contains(position)) {
            shipName = "Cruiser"
        } else if (_submarine[player].contains(position)) {
            shipName = "Submarine"
        } else if (_destroyer[player].contains(position)) {
            shipName = "Destroyer"
        } else {
            shipName = ""
        }
        return shipName
    }

    fun getPlayerBoard(): Array<Array<CellState>> {
        val playerBoard = Array(10, { Array(10, { CellState.WATER }) })
        val player: Int
        if (_gameState == GameState.PLAYER1_TURN) {
            player = 0
        } else {
            player = 1
        }
        for (i in 0..9) {
            for (j in 0..9) {
                val state = _gameBoards[player].get(i * 10 + j)
                playerBoard[i][j] = state ?: CellState.WATER
            }
        }
        return playerBoard
    }

    fun getOpponentBoard(): Array<Array<CellState>> {
        val opponentBoard = Array(10, { Array(10, { CellState.WATER }) })
        val player: Int
        if (_gameState == GameState.PLAYER1_TURN) {
            player = 1
        } else {
            player = 0
        }
        for (i in 0..9) {
            for (j in 0..9) {
                val state = _gameBoards[player].get(i * 10 + j)
                opponentBoard[i][j] = state ?: CellState.WATER
            }
        }
        return opponentBoard
    }

    fun shipsRemaining(player: Int): Int {
        var ships: Int = 0
        // Just check the first position of each ship, since all position change to sunk when the ship sinks.
        if (_gameBoards[player].get(_carrier[player][0]) != CellState.SUNK) ships++
        if (_gameBoards[player].get(_battleship[player][0]) != CellState.SUNK) ships++
        if (_gameBoards[player].get(_cruiser[player][0]) != CellState.SUNK) ships++
        if (_gameBoards[player].get(_submarine[player][0]) != CellState.SUNK) ships++
        if (_gameBoards[player].get(_destroyer[player][0]) != CellState.SUNK) ships++
        return ships
    }

    private fun nextPlayer(cell: Int) {
        if (_gameState == GameState.PLAYER1_TURN) {
            _gameState = GameState.PLAYER2_TURN
        } else {
            _gameState = GameState.PLAYER1_TURN
        }
        onUpdateGameStateListener?.onStateChanged(_gameState, cell)
    }

    private fun gameOver(cell: Int) {
        if (_gameState == GameState.PLAYER1_TURN) {
            _gameState = GameState.PLAYER1_WIN
        } else {
            _gameState = GameState.PLAYER2_WIN
        }
        onUpdateGameStateListener?.onStateChanged(_gameState, cell)
    }

    fun processMove(cell: Int) {
        // Player is opposite what you might expect because it's the player's board we are checking against.
        val player: Int = if (_gameState == GameState.PLAYER1_TURN) 1 else 0
//        Log.e("GameObject processMove", "Processing move at index $cell against player ${player + 1}")
        if (_gameBoards[player].get(cell) != null) {
            if (_gameBoards[player].get(cell) == CellState.SHIP) {
                _gameBoards[player].put(cell, CellState.HIT)
                if (!checkIfSunk(player, cell)) {
                    onUpdateGameStateListener?.onPositionUpdate(player, cell, CellState.HIT)
                    nextPlayer(cell)
                } else {
                    // Don't call onPositionUpdate in this case, because checkIfSunk will already have.
                    if (allShipsSunk(player)) {
                        gameOver(cell)
                    } else {
                        nextPlayer(cell)
                    }
                }
            } else {
                Log.e("Process Move", "Ignoring Player ${player + 1}'s move at position $cell.")
            }
        } else {
            _gameBoards[player].put(cell, CellState.MISS)
            onUpdateGameStateListener?.onPositionUpdate(player, cell, CellState.MISS)
            nextPlayer(cell)
        }
    }

    private fun checkIfSunk(player: Int, cell: Int): Boolean {
        if (_carrier[player].contains(cell)) {
            _carrier[player].forEach {
                if (_gameBoards[player].get(it) != CellState.HIT) {
                    return false
                }
            }
            Log.e("Ship Sunk", "Player ${player + 1}'s carrier sunk!")
            shipSunk(player, _carrier[player])
            return true
        } else if (_battleship[player].contains(cell)) {
            _battleship[player].forEach {
                if (_gameBoards[player].get(it) != CellState.HIT) {
                    return false
                }
            }
            Log.e("Ship Sunk", "Player ${player + 1}'s battleship sunk!")
            shipSunk(player, _battleship[player])
            return true
        } else if (_cruiser[player].contains(cell)) {
            _cruiser[player].forEach {
                if (_gameBoards[player].get(it) != CellState.HIT) {
                    return false
                }
            }
            Log.e("Ship Sunk", "Player ${player + 1}'s cruiser sunk!")
            shipSunk(player, _cruiser[player])
            return true
        } else if (_submarine[player].contains(cell)) {
            _submarine[player].forEach {
                if (_gameBoards[player].get(it) != CellState.HIT) {
                    return false
                }
            }
            Log.e("Ship Sunk", "Player ${player + 1}'s submarine sunk!")
            shipSunk(player, _submarine[player])
            return true
        } else if (_destroyer[player].contains(cell)) {
            _destroyer[player].forEach {
                if (_gameBoards[player].get(it) != CellState.HIT) {
                    return false
                }
            }
            Log.e("Ship Sunk", "Player ${player + 1}'s destroyer sunk!")
            shipSunk(player, _destroyer[player])
            return true
        } else {
            Log.e("Ship Sunk ERROR", "How did you get here?")
            return false
        }
    }

    private fun shipSunk(player: Int, ship: IntArray) {
        ship.forEach {
            _gameBoards[player].put(it, CellState.SUNK)
            onUpdateGameStateListener?.onPositionUpdate(player, it, CellState.SUNK)
        }
    }

    private fun allShipsSunk(player: Int): Boolean {
        _carrier[player].forEach {
            if (_gameBoards[player].get(it) != CellState.SUNK) {
                return false
            }
        }
        _battleship[player].forEach {
            if (_gameBoards[player].get(it) != CellState.SUNK) {
                return false
            }
        }
        _cruiser[player].forEach {
            if (_gameBoards[player].get(it) != CellState.SUNK) {
                return false
            }
        }
        _submarine[player].forEach {
            if (_gameBoards[player].get(it) != CellState.SUNK) {
                return false
            }
        }
        _destroyer[player].forEach {
            if (_gameBoards[player].get(it) != CellState.SUNK) {
                return false
            }
        }
        return true
    }

    private fun logBoard(player: Int) {
        Log.e("Board Output Begin", "Board Values: ")
        for (i in 0..9) {
            val builder = StringBuilder()
            builder.append("Row $i: ")
            for (j in 0..9) {
                val index = i * 10 + j
                if (_gameBoards[player].get(index) != null) {
                    if (_carrier[player].contains(index)) {
                        builder.append(" A")
                    } else if (_battleship[player].contains(index)) {
                        builder.append(" B")
                    } else if (_cruiser[player].contains(index)){
                        builder.append(" C")
                    } else if (_submarine[player].contains(index)) {
                        builder.append(" S")
                    } else if (_destroyer[player].contains(index)) {
                        builder.append(" D")
                    } else {
                        builder.append(" ?")
                    }
                } else {
                    builder.append(" -")
                }
            }
            val rowString: String = builder.toString()
            Log.e("Board Output", rowString)
        }
        Log.e("Board Output End", "Board Output Complete")
    }

}