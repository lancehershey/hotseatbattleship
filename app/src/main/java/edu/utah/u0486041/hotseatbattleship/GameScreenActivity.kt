package edu.utah.u0486041.hotseatbattleship

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.FrameLayout

/**
 * Created by Lance on 10/27/2016.
 */
class GameScreenActivity : AppCompatActivity(), GameObject.OnUpdateGameStateListener,
        CellView.OnCellSelectedListener, View.OnClickListener {
    companion object {
        var GAME_FRAME_ID: Int = 101
    }

    private var gameScreen: GameScreen? = null
    private var overlayScreen: OverlayScreen? = null

    private lateinit var game: GameObject
    private lateinit var gameLayout: FrameLayout

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (intent.hasExtra(R.string.GAME_KEY.toString())) {
            val gameIndex = intent.extras.getInt(R.string.GAME_KEY.toString())
            Log.e("Game Open", "Opened game $gameIndex!")
            GameManager.currentGame = gameIndex
        } else {
            GameManager.startGame()
            Log.e("Game Create", "Created new game!")
        }

        game = GameManager.getGame(GameManager.currentGame)
        game.setOnUpdateGameStateListener(this)

        gameLayout = FrameLayout(this)
        setContentView(gameLayout)

        gameLayout.id = GAME_FRAME_ID

        // This extra show game before the overlay is a hack that prevents the overlay from being
        // visible behind the game boards, which looked really bad.
        showGame()
        val player: Int = if (game.getGameState() == GameObject.GameState.PLAYER1_TURN) 1 else 2
        showOverlay("Please hand the device to player $player\n\n" +
                "Player $player: Tap to continue")
    }

    private fun showGame() {
        val transaction = supportFragmentManager.beginTransaction()
        if (supportFragmentManager.findFragmentByTag(getString(R.string.GAMESCREEN_FRAGMENT_TAG)) != null) {
            gameScreen = supportFragmentManager.findFragmentByTag(getString(R.string.GAMESCREEN_FRAGMENT_TAG)) as GameScreen
            transaction.replace(gameLayout.id, gameScreen)
        } else {
            gameScreen = GameScreen.newInstance()
            transaction.add(gameLayout.id, gameScreen, getString(R.string.GAMESCREEN_FRAGMENT_TAG))
        }
        transaction.commit()
    }

    private fun showOverlay(msg: String) {
        val transaction = supportFragmentManager.beginTransaction()
        if (supportFragmentManager.findFragmentByTag(getString(R.string.OVERLAY_FRAGMENT_TAG)) != null) {
            overlayScreen = supportFragmentManager.findFragmentByTag(getString(R.string.OVERLAY_FRAGMENT_TAG)) as OverlayScreen
            transaction.replace(gameLayout.id, overlayScreen)
        } else {
            overlayScreen = OverlayScreen.newInstance()
            transaction.add(gameLayout.id, overlayScreen, getString(R.string.OVERLAY_FRAGMENT_TAG))
        }
        val args = Bundle()
        args.putString(R.string.OVERLAY_MESSAGE_KEY.toString(), msg)
        overlayScreen?.arguments = args
        transaction.commit()
    }

    override fun onClick(view: View?) {
        if (game.getGameState() == GameObject.GameState.PLAYER1_WIN || game.getGameState() == GameObject.GameState.PLAYER2_WIN) {
            val gameListIntent: Intent = Intent(this, GameListActivity::class.java)
            startActivity(gameListIntent)
        }
        showGame()
        gameScreen?.updateBoard(game)
    }

    override fun onCellSelected(cellView: CellView) {
        val index = cellView.cellCoordinates.y * 10 + cellView.cellCoordinates.x
        game.processMove(index)
    }

    override fun onPositionUpdate(player: Int, position: Int, state: GameObject.CellState) {
        gameScreen?.updatePosition(player, position % 10, position / 10, state)
    }

    override fun onStateChanged(state: GameObject.GameState, cell: Int) {
        GameManager.saveFile(this)
        val msg: String
        when(state) {
        // This means player 2 just took their turn and it is becoming player 1's turn.
            GameObject.GameState.PLAYER1_TURN -> {
                if (game.getCellState(0, cell) == GameObject.CellState.SUNK) {
                    msg = "You sunk Player 1's ${game.getShipName(0, cell)}!\n" +
                            "Please hand the device to Player 1\n\n" +
                            "Player 1: Tap to continue"
                } else {
                    msg = "Your move at (${cell % 10}, ${cell / 10}) was a ${game.getCellState(0, cell)}!\n" +
                            "Please hand the device to Player 1\n\n" +
                            "Player 1: Tap to continue"
                }
                showOverlay(msg)
            }
            GameObject.GameState.PLAYER2_TURN -> {
                if (game.getCellState(1, cell) == GameObject.CellState.SUNK) {
                    msg = "You sunk Player 2's ${game.getShipName(1, cell)}!\n" +
                            "Please hand the device to Player 2\n\n" +
                            "Player 2: Tap to continue"
                } else {
                    msg = "Your move at (${cell % 10}, ${cell / 10}) was a ${game.getCellState(1, cell)}!\n" +
                            "Please hand the device to Player 2\n\n" +
                            "Player 2: Tap to continue"
                }
                showOverlay(msg)
            }
            GameObject.GameState.PLAYER1_WIN -> {
                msg = "Player 1 wins!\n" +
                        "Tap to return to game list"
                showOverlay(msg)
            }
            GameObject.GameState.PLAYER2_WIN -> {
                msg = "Player 2 wins!\n" +
                        "Tap to return to game list"
                showOverlay(msg)
            }
            else -> Unit
        }
    }

}