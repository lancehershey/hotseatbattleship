package edu.utah.u0486041.hotseatbattleship

import android.content.Context
import android.util.Log
import org.json.JSONArray
import java.io.BufferedReader
import java.io.File
import java.io.InputStreamReader

/**
 * Created by Lance on 10/24/2016.
 */
object GameManager {
    private val _gameObjectList = mutableListOf<GameObject>()
    var currentGame: Int = 0

    val gameCount: Int
        get() {
            return _gameObjectList.size
        }

    fun startGame() {
        _gameObjectList.add(GameObject())
        currentGame = _gameObjectList.size - 1
    }

    fun getGame(gameIndex: Int): GameObject {
        currentGame = gameIndex
        return _gameObjectList[gameIndex]
    }

    fun deleteGame(gameIndex: Int) {
        _gameObjectList.removeAt(gameIndex)
    }

    private fun fromJSON(JSON: String) {
        _gameObjectList.clear()
        val gameListJSON = JSONArray(JSON)
        for (i in 0..gameListJSON.length() - 1) {
            val game = GameObject.fromJSON(gameListJSON[i].toString())
            if (game != null) {
                _gameObjectList.add(game)
            }
        }
    }

    private fun toJSON(): String {
        val gameListJSON = JSONArray()
        for (i in 0..gameCount - 1) {
            gameListJSON.put(_gameObjectList[i].toJSON())
        }
        Log.e("Game List JSON", gameListJSON.toString())
        return gameListJSON.toString()
    }

    fun saveFile(context: Context) {
        val file = File(context.filesDir, R.string.save_file_name.toString())
        try {
            val fileOutputStream = context.openFileOutput(file.name, Context.MODE_PRIVATE)
            fileOutputStream.write(GameManager.toJSON().toByteArray())
            fileOutputStream.close()
        } catch (exception: Exception) {
            exception.printStackTrace()
        }
    }

    fun loadFile(context: Context) {
        val file = File(context.filesDir, R.string.save_file_name.toString())
        if (file.exists()) {
            try {
                val reader = BufferedReader(InputStreamReader(context.openFileInput(file.name)))
                val builder = StringBuilder()
                var line: String? = reader.readLine()
                while (line != null) {
                    builder.append(line)
                    line = reader.readLine()
                }
                reader.close()
                GameManager.fromJSON(builder.toString())
            } catch (exception: Exception) {
                exception.printStackTrace()
            }
        }
    }

}
